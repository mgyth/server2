/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.web;

import java.io.IOException;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.apache.myfaces.custom.fileupload.UploadedFileDefaultFileImpl;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import server.entity.Prosjekter;
import server.web.util.PaginationHelper;

/**
 *
 * @author Mikael
 */
public class ProsjekterControllerTest {

    private UploadedFile uploadedFile;
    private static Prosjekter current;
    private int selectedItemIndex;

    public ProsjekterControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws IOException {
        // Opprett mockup fil for testSubmit
        final DiskFileItemFactory factory = new DiskFileItemFactory();
        FileItem fi = factory.createItem("field", "text", true, "Test.txt");
        fi.getOutputStream().write("testString".getBytes());
        fi.getOutputStream().close();
        uploadedFile = new UploadedFileDefaultFileImpl(fi);
        // opprett current objekt for testGetSelected
        current = new Prosjekter();
        current.setProsjektID(500);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of submit method, of class ProsjekterController.
     */
    @Test
    public void testSubmit() {
        System.out.println("submit");
        //sjekk at filen ikke er null
        assertNotNull(uploadedFile);
        //sjekk om filnavn er korrekt
        String resultFn = FilenameUtils.getName(uploadedFile.getName());
        String expResultFN = "Test.txt";
        assertEquals(expResultFN, resultFn);
        //sjekk om filtype er korrekt
        String resultCT = uploadedFile.getContentType();
        String expResultCT = "text";
        assertEquals(expResultCT, resultCT);
    }

    /**
     * Test of getSelected method, of class ProsjekterController. Test med
     * current objekt
     */
    @Test
    public void testGetSelected() {
        System.out.println("getSelected");
        ProsjekterController instance = new ProsjekterController();
        Prosjekter expResult = current;
        Prosjekter result = instance.getSelected();
        assertEquals(expResult, result);
    }

    /**
     * Test of getSelected method, of class ProsjekterController. Test uten
     * current objekt
     */
    @Test
    public void testGetSelectedNull() {
        System.out.println("getSelected");
        ProsjekterController instance = new ProsjekterController();
        Prosjekter expResult = new Prosjekter();
        Prosjekter result = instance.getSelected();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPagination method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testGetPagination() {
        System.out.println("getPagination");
        ProsjekterController instance = new ProsjekterController();
        PaginationHelper expResult = null;
        PaginationHelper result = instance.getPagination();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of prepareList method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testPrepareList() {
        System.out.println("prepareList");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.prepareList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of prepareView method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testPrepareView() {
        System.out.println("prepareView");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.prepareView();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of prepareCreate method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testPrepareCreate() {
        System.out.println("prepareCreate");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.prepareCreate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of create method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testCreate() {
        System.out.println("create");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.create();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of prepareEdit method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testPrepareEdit() {
        System.out.println("prepareEdit");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.prepareEdit();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testUpdate() {
        System.out.println("update");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.update();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of destroy method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testDestroy() {
        System.out.println("destroy");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.destroy();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of destroyAndView method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testDestroyAndView() {
        System.out.println("destroyAndView");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.destroyAndView();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItems method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testGetItems() {
        System.out.println("getItems");
        ProsjekterController instance = new ProsjekterController();
        DataModel expResult = null;
        DataModel result = instance.getItems();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of next method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testNext() {
        System.out.println("next");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.next();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of previous method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testPrevious() {
        System.out.println("previous");
        ProsjekterController instance = new ProsjekterController();
        String expResult = "";
        String result = instance.previous();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItemsAvailableSelectMany method, of class
     * ProsjekterController.
     */
    @Ignore
    @Test
    public void testGetItemsAvailableSelectMany() {
        System.out.println("getItemsAvailableSelectMany");
        ProsjekterController instance = new ProsjekterController();
        SelectItem[] expResult = null;
        SelectItem[] result = instance.getItemsAvailableSelectMany();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getItemsAvailableSelectOne method, of class ProsjekterController.
     */
    @Ignore
    @Test
    public void testGetItemsAvailableSelectOne() {
        System.out.println("getItemsAvailableSelectOne");
        ProsjekterController instance = new ProsjekterController();
        SelectItem[] expResult = null;
        SelectItem[] result = instance.getItemsAvailableSelectOne();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}

/*
 * Copyright 2012 Oracle and/or its affiliates.
 * All rights reserved.  You may not modify, use,
 * reproduce, or distribute this software except in
 * compliance with  the terms of the License at:
 * http://developers.sun.com/license/berkeley_license.html
 */


package client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import server.entity.Doerer;


public class testClient {
    public static final Logger logger = Logger.getLogger(
                testClient.class.getCanonicalName());

    public static void main(String[] args) {
        Client client = Client.create();

        // Define the URL for testing the example application
        WebResource webResource = client.resource("http://localhost:8080/Server356/webresources/Test");

        // Test the POST method
        String test = "1";

        
        ClientResponse response = webResource.type("text/plain").post(ClientResponse.class, test);

        logger.log(Level.INFO,"POST status: {0}",response.getStatus());

        if (response.getStatus() == 201) {
            logger.info("POST succeeded");
        } else {
            logger.info("POST failed");
        }

        // Test the GET method using content negotiation
        response = webResource.path("153")
                              .accept(MediaType.TEXT_PLAIN)
                              .get(ClientResponse.class);

        logger.log(Level.INFO, "GET status: {0}",response.getStatus());

        if (response.getStatus() == 200) {
            logger.log( Level.INFO, "GET succeeded, city is {0}");
        } else {
            logger.info("GET failed");
        }
        
        
        //Test GET i en objectklasse
        WebResource webResource2 = client.resource("http://localhost:8080/server356/webresources/server.entity.doerer");
        
        ClientResponse response2 = webResource2.path("113")
                              .accept(MediaType.APPLICATION_JSON)
                              .get(ClientResponse.class);
        
        Doerer enDoer = response2.getEntity(Doerer.class);
        
        logger.log(Level.INFO, "GET status: {0}",response2.getStatus());

        if (response2.getStatus() == 200) {
            logger.log(Level.INFO, "GET succeeded, door 113 is doorNr: " + enDoer.getDorNr() + " og doortype: " + enDoer.getRomtype());
        } else {
            logger.info("GET failed");
        }
        

    }
}

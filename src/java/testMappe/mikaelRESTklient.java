/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testMappe;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * Jersey
 * REST
 * client
 * generated
 * for
 * REST
 * resource:TestREST
 * [/Test]<br>
 *  USAGE:
 * <pre>
 *        mikaelRESTklient client = new mikaelRESTklient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author
 * Mikael
 */
public class mikaelRESTklient {
    private WebResource webResource;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/Server356/webresources";

    public mikaelRESTklient() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("Test");
    }

    public String createCustomer(Object requestEntity) throws UniformInterfaceException {
        return webResource.type(javax.ws.rs.core.MediaType.TEXT_PLAIN).post(String.class, requestEntity);
    }

    public String getString(String id) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path(java.text.MessageFormat.format("{0}", new Object[]{id}));
        return resource.accept(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public void close() {
        client.destroy();
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import server.entity.Endringer;
import server.entity.Version;

/**
 *
 * @author David
 */
@Stateless
@Path("server.entity.endringer")
public class EndringerFacadeREST extends AbstractFacade<Endringer> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    public EndringerFacadeREST() {
        super(Endringer.class);
    }
    
    
    @GET
    @Path("{version}")
    @Produces({"application/xml", "application/json"})
    public Endringer getEndringer(@PathParam("version") double version) {
        Endringer endring;
        endring = new Endringer(version);
        endring.setCurrentVersion(((Version)em.createNamedQuery("Version.findByVersionID").setParameter("versionID", 1).getSingleResult()).getVersion());
        
        endring.setAdresser((ArrayList)em.createNamedQuery("Adresse.findByVersion").setParameter("version", version).getResultList());
        endring.setBeslag((ArrayList)em.createNamedQuery("Beslagslister.findByVersion").setParameter("version", version).getResultList());
        endring.setDoerer((ArrayList)em.createNamedQuery("Doerer.findByVersion").setParameter("version", version));
        endring.setFirma((ArrayList)em.createNamedQuery("Firma.findByVersion").setParameter("version", version));
        endring.setKategorier((ArrayList)em.createNamedQuery("Kategorier.findByVersion").setParameter("version", version));
        endring.setKontakter((ArrayList)em.createNamedQuery("KontaktPersoner.findByVersion").setParameter("version", version));
        endring.setProsjekter((ArrayList)em.createNamedQuery("Prosjekter.findByVersion").setParameter("version", version));
        endring.setTegninger((ArrayList)em.createNamedQuery("Tegninger.findByVersion").setParameter("version", version));
        return endring;
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Endringer entity) {
        super.create(entity);
    }

    @PUT
    @Override
    @Consumes({"application/xml", "application/json"})
    public void edit(Endringer entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Double id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Endringer find(@PathParam("id") Double id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Endringer> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Endringer> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}

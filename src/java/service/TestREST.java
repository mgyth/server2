/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author David
 */
@Path("/Test")
public class TestREST {
    
/**
 * Customer Restful Service with CRUD methods
 *
 * @author markito <william.markito@oracle.com>
 */


    @GET
    @Path("{id}")
    @Produces({"text/plain"})
    public String getString(@PathParam("id") String customerId) {
        String id = customerId;
        return ("Vi fikk i det minste kontakt..." + id);
        
    }

    /**
     * createCustomer method based on
     * <code>CustomerType</code>
     *
     * @param customer
     * @return Response URI for the Customer added
     * @see Customer.java
     */
    @POST
    @Consumes({"text/plain"})
    public String createCustomer(String customer) {
        return customer;
    }
}
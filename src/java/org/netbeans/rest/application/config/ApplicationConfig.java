/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.Set;
import java.util.logging.Logger;
import javax.ws.rs.core.Application;

/**
 *
 * @author David
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return getRestResourceClasses();
    }
    private static final Logger LOG = Logger.getLogger(ApplicationConfig.class.getName());

    private Set<Class<?>> getRestResourceClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        resources.add(service.FirmaFacadeREST.class);
        resources.add(service.KategorierFacadeREST.class);
        resources.add(service.EndringerFacadeREST.class);
        resources.add(service.VersionFacadeREST.class);
        resources.add(service.DoererFacadeREST.class);
        resources.add(service.ProsjekterFacadeREST.class);
        resources.add(service.TestREST.class);
        resources.add(service.BeslagslisterFacadeREST.class);
        resources.add(service.ProsjektKategorierFacadeREST.class);
        resources.add(service.KontaktPersonerFacadeREST.class);
        resources.add(service.BrukereFacadeREST.class);
        resources.add(service.AdresseFacadeREST.class);
        resources.add(service.TegningerFacadeREST.class);
        return resources;
    }
    
}

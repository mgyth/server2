/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Brukere;
import server.entity.Prosjekter;

/**
 *
 * @author Mikael
 */
@Stateless
public class BrukereFacade extends AbstractFacade<Brukere> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void editProsjekt(Prosjekter entity) {
        getEntityManager().merge(entity);
    }

    public BrukereFacade() {
        super(Brukere.class);
    }
    private static final Logger LOG = Logger.getLogger(BrukereFacade.class.getName());
    
}

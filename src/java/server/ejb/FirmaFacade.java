/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Firma;

/**
 *
 * @author Mikael
 */
@Stateless
public class FirmaFacade extends AbstractFacade<Firma> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FirmaFacade() {
        super(Firma.class);
    }
    private static final Logger LOG = Logger.getLogger(FirmaFacade.class.getName());
    
}

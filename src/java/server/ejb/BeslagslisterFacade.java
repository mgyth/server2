/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Beslagslister;

/**
 *
 * @author Mikael
 */
@Stateless
public class BeslagslisterFacade extends AbstractFacade<Beslagslister> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BeslagslisterFacade() {
        super(Beslagslister.class);
    }
    private static final Logger LOG = Logger.getLogger(BeslagslisterFacade.class.getName());
    
}

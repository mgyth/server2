/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.ProsjektKategorier;

/**
 *
 * @author Mikael
 */
@Stateless
public class ProsjektKategorierFacade extends AbstractFacade<ProsjektKategorier> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProsjektKategorierFacade() {
        super(ProsjektKategorier.class);
    }
    private static final Logger LOG = Logger.getLogger(ProsjektKategorierFacade.class.getName());
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Kategorier;

/**
 *
 * @author Mikael
 */
@Stateless
public class KategorierFacade extends AbstractFacade<Kategorier> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KategorierFacade() {
        super(Kategorier.class);
    }
    private static final Logger LOG = Logger.getLogger(KategorierFacade.class.getName());
    
}

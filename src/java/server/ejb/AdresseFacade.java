/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Adresse;

/**
 *
 * @author Mikael
 */
@Stateless
public class AdresseFacade extends AbstractFacade<Adresse> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    

    public AdresseFacade() {
        super(Adresse.class);
    }
    private static final Logger LOG = Logger.getLogger(AdresseFacade.class.getName());
    
}

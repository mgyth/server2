/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.ejb;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import server.entity.Brukere;
import server.entity.KontaktPersoner;
import server.entity.Prosjekter;

/**
 *
 * @author Mikael
 */
@Stateless
public class ProsjekterFacade extends AbstractFacade<Prosjekter> {
    @PersistenceContext(unitName = "ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public void editKontaktPerson(KontaktPersoner entity) {
        getEntityManager().merge(entity);
    }
    
    public void editBrukere(Brukere entity) {
        getEntityManager().merge(entity);
    }

    public ProsjekterFacade() {
        super(Prosjekter.class);
    }
    private static final Logger LOG = Logger.getLogger(ProsjekterFacade.class.getName());
    
}

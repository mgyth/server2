/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Doerer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Doerer.findAll", query = "SELECT d FROM Doerer d"),
    @NamedQuery(name = "Doerer.findByIDDor", query = "SELECT d FROM Doerer d WHERE d.iDDor = :iDDor"),
    @NamedQuery(name = "Doerer.findByDorNr", query = "SELECT d FROM Doerer d WHERE d.dorNr = :dorNr"),
    @NamedQuery(name = "Doerer.findByBygning", query = "SELECT d FROM Doerer d WHERE d.bygning = :bygning"),
    @NamedQuery(name = "Doerer.findByEtasje", query = "SELECT d FROM Doerer d WHERE d.etasje = :etasje"),
    @NamedQuery(name = "Doerer.findByRomtype", query = "SELECT d FROM Doerer d WHERE d.romtype = :romtype"),
    @NamedQuery(name = "Doerer.findById", query = "SELECT d FROM Doerer d WHERE d.id = :id"),
    @NamedQuery(name = "Doerer.findByDoertype", query = "SELECT d FROM Doerer d WHERE d.doertype = :doertype"),
    @NamedQuery(name = "Doerer.findByDoerKonstruksjon1", query = "SELECT d FROM Doerer d WHERE d.doerKonstruksjon1 = :doerKonstruksjon1"),
    @NamedQuery(name = "Doerer.findByDoerKonstruksjon2", query = "SELECT d FROM Doerer d WHERE d.doerKonstruksjon2 = :doerKonstruksjon2"),
    @NamedQuery(name = "Doerer.findByFormat", query = "SELECT d FROM Doerer d WHERE d.format = :format"),
    @NamedQuery(name = "Doerer.findBySlagretning", query = "SELECT d FROM Doerer d WHERE d.slagretning = :slagretning"),
    @NamedQuery(name = "Doerer.findByGlassapning", query = "SELECT d FROM Doerer d WHERE d.glassapning = :glassapning"),
    @NamedQuery(name = "Doerer.findByFloy", query = "SELECT d FROM Doerer d WHERE d.floy = :floy"),
    @NamedQuery(name = "Doerer.findByOverflatedoer", query = "SELECT d FROM Doerer d WHERE d.overflatedoer = :overflatedoer"),
    @NamedQuery(name = "Doerer.findByKarm", query = "SELECT d FROM Doerer d WHERE d.karm = :karm"),
    @NamedQuery(name = "Doerer.findByOverflatekarm", query = "SELECT d FROM Doerer d WHERE d.overflatekarm = :overflatekarm"),
    @NamedQuery(name = "Doerer.findByTerskel", query = "SELECT d FROM Doerer d WHERE d.terskel = :terskel"),
    @NamedQuery(name = "Doerer.findBySistendret", query = "SELECT d FROM Doerer d WHERE d.sistendret = :sistendret"),
    @NamedQuery(name = "Doerer.findByFerdig", query = "SELECT d FROM Doerer d WHERE d.ferdig = :ferdig"),
    @NamedQuery(name = "Doerer.findByUtforing", query = "SELECT d FROM Doerer d WHERE d.utforing = :utforing"),
    @NamedQuery(name = "Doerer.findByDytting", query = "SELECT d FROM Doerer d WHERE d.dytting = :dytting"),
    @NamedQuery(name = "Doerer.findByFuging", query = "SELECT d FROM Doerer d WHERE d.fuging = :fuging"),
    @NamedQuery(name = "Doerer.findByListverk", query = "SELECT d FROM Doerer d WHERE d.listverk = :listverk"),
    @NamedQuery(name = "Doerer.findByMontert", query = "SELECT d FROM Doerer d WHERE d.montert = :montert"),
    @NamedQuery(name = "Doerer.findByRevisjon", query = "SELECT d FROM Doerer d WHERE d.revisjon = :revisjon"),
    @NamedQuery(name = "Doerer.findByVersion", query = "SELECT d FROM Doerer d WHERE d.version = :version"),
    @NamedQuery(name = "Doerer.findByVersion", query = "SELECT d FROM Doerer d WHERE d.version = :version")})
public class Doerer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_Dor")
    private Integer iDDor;
    @Basic(optional = false)
    
    @Size( max = 20)
    @Column(name = "DorNr")
    private String dorNr;
    @Size(max = 10)
    @Column(name = "Bygning")
    private String bygning;
    @Size(max = 3)
    @Column(name = "Etasje")
    private String etasje;
    @Size(max = 45)
    @Column(name = "Romtype")
    private String romtype;
    @Size(max = 10)
    @Column(name = "ID")
    private String id;
    @Size(max = 20)
    @Column(name = "Doertype")
    private String doertype;
    @Size(max = 15)
    @Column(name = "DoerKonstruksjon1")
    private String doerKonstruksjon1;
    @Size(max = 20)
    @Column(name = "DoerKonstruksjon2")
    private String doerKonstruksjon2;
    @Size(max = 20)
    @Column(name = "Format")
    private String format;
    @Size(max = 5)
    @Column(name = "Slagretning")
    private String slagretning;
    @Size(max = 10)
    @Column(name = "Glassapning")
    private String glassapning;
    @Size(max = 10)
    @Column(name = "Floy")
    private String floy;
    @Size(max = 20)
    @Column(name = "Overflate_doer")
    private String overflatedoer;
    @Size(max = 20)
    @Column(name = "Karm")
    private String karm;
    @Size(max = 20)
    @Column(name = "Overflate_karm")
    private String overflatekarm;
    @Size(max = 20)
    @Column(name = "Terskel")
    private String terskel;
    @Column(name = "Sist_endret")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sistendret;
    @Column(name = "ferdig")
    private Boolean ferdig;
    @Size(max = 45)
    @Column(name = "Utforing")
    private String utforing;
    @Size(max = 45)
    @Column(name = "Dytting")
    private String dytting;
    @Size(max = 45)
    @Column(name = "Fuging")
    private String fuging;
    @Size(max = 45)
    @Column(name = "Listverk")
    private String listverk;
    @Lob
    @Column(name = "Merknad")
    private String merknad;
    @Column(name = "Montert")
    @Temporal(TemporalType.TIMESTAMP)
    private Date montert;
    @Size(max = 45)
    @Column(name = "Revisjon")
    private String revisjon;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;
    @JoinColumn(name = "ProsjektID_fk", referencedColumnName = "ProsjektID")
    @ManyToOne
    private Prosjekter prosjektIDfk;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doridfk")
    private Collection<Beslagslister> beslagslisterCollection;

    public Doerer() {
    }

    public Doerer(Integer iDDor) {
        this.iDDor = iDDor;
    }

    public Doerer(Integer iDDor, String dorNr) {
        this.iDDor = iDDor;
        this.dorNr = dorNr;
    }

    public Integer getIDDor() {
        return iDDor;
    }

    public void setIDDor(Integer iDDor) {
        this.iDDor = iDDor;
    }

    public String getDorNr() {
        return dorNr;
    }

    public void setDorNr(String dorNr) {
        this.dorNr = dorNr;
    }

    public String getBygning() {
        return bygning;
    }

    public void setBygning(String bygning) {
        this.bygning = bygning;
    }

    public String getEtasje() {
        return etasje;
    }

    public void setEtasje(String etasje) {
        this.etasje = etasje;
    }

    public String getRomtype() {
        return romtype;
    }

    public void setRomtype(String romtype) {
        this.romtype = romtype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoertype() {
        return doertype;
    }

    public void setDoertype(String doertype) {
        this.doertype = doertype;
    }

    public String getDoerKonstruksjon1() {
        return doerKonstruksjon1;
    }

    public void setDoerKonstruksjon1(String doerKonstruksjon1) {
        this.doerKonstruksjon1 = doerKonstruksjon1;
    }

    public String getDoerKonstruksjon2() {
        return doerKonstruksjon2;
    }

    public void setDoerKonstruksjon2(String doerKonstruksjon2) {
        this.doerKonstruksjon2 = doerKonstruksjon2;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSlagretning() {
        return slagretning;
    }

    public void setSlagretning(String slagretning) {
        this.slagretning = slagretning;
    }

    public String getGlassapning() {
        return glassapning;
    }

    public void setGlassapning(String glassapning) {
        this.glassapning = glassapning;
    }

    public String getFloy() {
        return floy;
    }

    public void setFloy(String floy) {
        this.floy = floy;
    }

    public String getOverflatedoer() {
        return overflatedoer;
    }

    public void setOverflatedoer(String overflatedoer) {
        this.overflatedoer = overflatedoer;
    }

    public String getKarm() {
        return karm;
    }

    public void setKarm(String karm) {
        this.karm = karm;
    }

    public String getOverflatekarm() {
        return overflatekarm;
    }

    public void setOverflatekarm(String overflatekarm) {
        this.overflatekarm = overflatekarm;
    }

    public String getTerskel() {
        return terskel;
    }

    public void setTerskel(String terskel) {
        this.terskel = terskel;
    }

    public Date getSistendret() {
        return sistendret;
    }

    public void setSistendret(Date sistendret) {
        this.sistendret = sistendret;
    }

    public Boolean getFerdig() {
        return ferdig;
    }

    public void setFerdig(Boolean ferdig) {
        this.ferdig = ferdig;
    }

    public String getUtforing() {
        return utforing;
    }

    public void setUtforing(String utforing) {
        this.utforing = utforing;
    }

    public String getDytting() {
        return dytting;
    }

    public void setDytting(String dytting) {
        this.dytting = dytting;
    }

    public String getFuging() {
        return fuging;
    }

    public void setFuging(String fuging) {
        this.fuging = fuging;
    }

    public String getListverk() {
        return listverk;
    }

    public void setListverk(String listverk) {
        this.listverk = listverk;
    }

    public String getMerknad() {
        return merknad;
    }

    public void setMerknad(String merknad) {
        this.merknad = merknad;
    }

    public Date getMontert() {
        return montert;
    }

    public void setMontert(Date montert) {
        this.montert = montert;
    }

    public String getRevisjon() {
        return revisjon;
    }

    public void setRevisjon(String revisjon) {
        this.revisjon = revisjon;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Prosjekter getProsjektIDfk() {
        return prosjektIDfk;
    }

    public void setProsjektIDfk(Prosjekter prosjektIDfk) {
        this.prosjektIDfk = prosjektIDfk;
    }

    @XmlTransient
    public Collection<Beslagslister> getBeslagslisterCollection() {
        return beslagslisterCollection;
    }

    public void setBeslagslisterCollection(Collection<Beslagslister> beslagslisterCollection) {
        this.beslagslisterCollection = beslagslisterCollection;
    }
    
    public void addBeslagslisterCollection(Beslagslister beslag){
        this.beslagslisterCollection.add(beslag);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDDor != null ? iDDor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Doerer)) {
            return false;
        }
        Doerer other = (Doerer) object;
        if ((this.iDDor == null && other.iDDor != null) || (this.iDDor != null && !this.iDDor.equals(other.iDDor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.dorNr;
    }
    private static final Logger LOG = Logger.getLogger(Doerer.class.getName());
    
}

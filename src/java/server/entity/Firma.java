/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Firma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Firma.findAll", query = "SELECT f FROM Firma f"),
    @NamedQuery(name = "Firma.findByIdFirma", query = "SELECT f FROM Firma f WHERE f.idFirma = :idFirma"),
    @NamedQuery(name = "Firma.findByFirmanavn", query = "SELECT f FROM Firma f WHERE f.firmanavn = :firmanavn"),
    @NamedQuery(name = "Firma.findByTlf", query = "SELECT f FROM Firma f WHERE f.tlf = :tlf"),
    @NamedQuery(name = "Firma.findByEpost", query = "SELECT f FROM Firma f WHERE f.epost = :epost"),
    @NamedQuery(name = "Firma.findByVersion", query = "SELECT f FROM Firma f WHERE f.version > :version")})
public class Firma implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFirma")
    private Integer idFirma;
    @Size(max = 65)
    @Column(name = "Firmanavn")
    private String firmanavn;
    @Size(max = 20)
    @Column(name = "Tlf")
    private String tlf;
    @Size(max = 65)
    @Column(name = "Epost")
    private String epost;
    @OneToMany(mappedBy = "firmaid", fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<KontaktPersoner> kontaktPersonerCollection;
    @JoinColumn(name = "Adresse", referencedColumnName = "idAdresse")
    @ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Adresse adresse;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Firma() {
    }

    public Firma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public Integer getIdFirma() {
        return idFirma;
    }

    public void setIdFirma(Integer idFirma) {
        this.idFirma = idFirma;
    }

    public String getFirmanavn() {
        return firmanavn;
    }

    public void setFirmanavn(String firmanavn) {
        this.firmanavn = firmanavn;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    public String getEpost() {
        return epost;
    }

    public void setEpost(String epost) {
        this.epost = epost;
    }

    @XmlTransient
    public Collection<KontaktPersoner> getKontaktPersonerCollection() {
        return kontaktPersonerCollection;
    }

    public void setKontaktPersonerCollection(Collection<KontaktPersoner> kontaktPersonerCollection) {
        this.kontaktPersonerCollection = kontaktPersonerCollection;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFirma != null ? idFirma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Firma)) {
            return false;
        }
        Firma other = (Firma) object;
        if ((this.idFirma == null && other.idFirma != null) || (this.idFirma != null && !this.idFirma.equals(other.idFirma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return firmanavn;
    }
    private static final Logger LOG = Logger.getLogger(Firma.class.getName());
    
}

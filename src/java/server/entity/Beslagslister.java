/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Beslagslister")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Beslagslister.findAll", query = "SELECT b FROM Beslagslister b"),
    @NamedQuery(name = "Beslagslister.findByIDBeslagsliste", query = "SELECT b FROM Beslagslister b WHERE b.iDBeslagsliste = :iDBeslagsliste"),
    @NamedQuery(name = "Beslagslister.findByBeslagnavn", query = "SELECT b FROM Beslagslister b WHERE b.beslagnavn = :beslagnavn"),
    @NamedQuery(name = "Beslagslister.findByOverflate", query = "SELECT b FROM Beslagslister b WHERE b.overflate = :overflate"),
    @NamedQuery(name = "Beslagslister.findByMontert", query = "SELECT b FROM Beslagslister b WHERE b.montert = :montert"),
    @NamedQuery(name = "Beslagslister.findBySistendret", query = "SELECT b FROM Beslagslister b WHERE b.sistendret = :sistendret"),
    @NamedQuery(name = "Beslagslister.findByVersion", query = "SELECT b FROM Beslagslister b WHERE b.version > :version")})
public class Beslagslister implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "IDBeslagsliste")
    private Integer iDBeslagsliste;
    @Size(max = 65)
    @Column(name = "Beslagnavn")
    private String beslagnavn;
    @Size(max = 45)
    @Column(name = "Overflate")
    private String overflate;
    @Column(name = "Montert")
    private Boolean montert;
    @Column(name = "Sist_endret")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sistendret;
    @JoinColumn(name = "Montert_av_fk", referencedColumnName = "idBrukere")
    @ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Brukere montertavfk;
    @JoinColumn(name = "Dor_id_fk", referencedColumnName = "ID_Dor")
    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Doerer doridfk;
    @JoinColumn(name = "Kategori_id_fk", referencedColumnName = "idKategori")
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private Kategorier kategoriidfk;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Beslagslister() {
    }

    public Beslagslister(Integer iDBeslagsliste) {
        this.iDBeslagsliste = iDBeslagsliste;
    }

    public Integer getIDBeslagsliste() {
        return iDBeslagsliste;
    }

    public void setIDBeslagsliste(Integer iDBeslagsliste) {
        this.iDBeslagsliste = iDBeslagsliste;
    }

    public String getBeslagnavn() {
        return beslagnavn;
    }

    public void setBeslagnavn(String beslagnavn) {
        this.beslagnavn = beslagnavn;
    }

    public String getOverflate() {
        return overflate;
    }

    public void setOverflate(String overflate) {
        this.overflate = overflate;
    }

    public Boolean getMontert() {
        return montert;
    }

    public void setMontert(Boolean montert) {
        this.montert = montert;
    }

    public Date getSistendret() {
        return sistendret;
    }

    public void setSistendret(Date sistendret) {
        this.sistendret = sistendret;
    }

    public Brukere getMontertavfk() {
        return montertavfk;
    }

    public void setMontertavfk(Brukere montertavfk) {
        this.montertavfk = montertavfk;
    }

    public Doerer getDoridfk() {
        return doridfk;
    }

    public void setDoridfk(Doerer doridfk) {
        this.doridfk = doridfk;
    }

    public Kategorier getKategoriidfk() {
        return kategoriidfk;
    }

    public void setKategoriidfk(Kategorier kategoriidfk) {
        this.kategoriidfk = kategoriidfk;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDBeslagsliste != null ? iDBeslagsliste.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Beslagslister)) {
            return false;
        }
        Beslagslister other = (Beslagslister) object;
        if ((this.iDBeslagsliste == null && other.iDBeslagsliste != null) || (this.iDBeslagsliste != null && !this.iDBeslagsliste.equals(other.iDBeslagsliste))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "server.entity.Beslagslister[ iDBeslagsliste=" + iDBeslagsliste + " ]";
    }
    private static final Logger LOG = Logger.getLogger(Beslagslister.class.getName());
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Adresse")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adresse.findAll", query = "SELECT a FROM Adresse a"),
    @NamedQuery(name = "Adresse.findByIdAdresse", query = "SELECT a FROM Adresse a WHERE a.idAdresse = :idAdresse"),
    @NamedQuery(name = "Adresse.findByAdr1", query = "SELECT a FROM Adresse a WHERE a.adr1 = :adr1"),
    @NamedQuery(name = "Adresse.findByAdr2", query = "SELECT a FROM Adresse a WHERE a.adr2 = :adr2"),
    @NamedQuery(name = "Adresse.findByAdr3", query = "SELECT a FROM Adresse a WHERE a.adr3 = :adr3"),
    @NamedQuery(name = "Adresse.findByPostNr", query = "SELECT a FROM Adresse a WHERE a.postNr = :postNr"),
    @NamedQuery(name = "Adresse.findByPostSted", query = "SELECT a FROM Adresse a WHERE a.postSted = :postSted"),
    @NamedQuery(name = "Adresse.findByVersion", query = "SELECT a FROM Adresse a WHERE a.version > :version")})
public class Adresse implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAdresse")
    private Integer idAdresse;
    @Size(max = 45)
    @Column(name = "adr1")
    private String adr1;
    @Size(max = 45)
    @Column(name = "adr2")
    private String adr2;
    @Size(max = 45)
    @Column(name = "adr3")
    private String adr3;
    @Column(name = "postNr")
    private Integer postNr;
    @Size(max = 45)
    @Column(name = "postSted")
    private String postSted;
    @OneToMany(mappedBy = "adresse", fetch = FetchType.LAZY)
    private Collection<KontaktPersoner> kontaktPersonerCollection;
    @OneToMany(mappedBy = "adresse", fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<Firma> firmaCollection;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Adresse() {
    }

    public Adresse(Integer idAdresse) {
        this.idAdresse = idAdresse;
    }

    public Integer getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(Integer idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getAdr1() {
        return adr1;
    }

    public void setAdr1(String adr1) {
        this.adr1 = adr1;
    }

    public String getAdr2() {
        return adr2;
    }

    public void setAdr2(String adr2) {
        this.adr2 = adr2;
    }

    public String getAdr3() {
        return adr3;
    }

    public void setAdr3(String adr3) {
        this.adr3 = adr3;
    }

    public Integer getPostNr() {
        return postNr;
    }

    public void setPostNr(Integer postNr) {
        this.postNr = postNr;
    }

    public String getPostSted() {
        return postSted;
    }

    public void setPostSted(String postSted) {
        this.postSted = postSted;
    }

    @XmlTransient
    public Collection<KontaktPersoner> getKontaktPersonerCollection() {
        return kontaktPersonerCollection;
    }

    public void setKontaktPersonerCollection(Collection<KontaktPersoner> kontaktPersonerCollection) {
        this.kontaktPersonerCollection = kontaktPersonerCollection;
    }

    @XmlTransient
    public Collection<Firma> getFirmaCollection() {
        return firmaCollection;
    }

    public void setFirmaCollection(Collection<Firma> firmaCollection) {
        this.firmaCollection = firmaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdresse != null ? idAdresse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adresse)) {
            return false;
        }
        Adresse other = (Adresse) object;
        if ((this.idAdresse == null && other.idAdresse != null) || (this.idAdresse != null && !this.idAdresse.equals(other.idAdresse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return adr1 + ", " + postNr + " " + postSted;
    }
    private static final Logger LOG = Logger.getLogger(Adresse.class.getName());
    
}

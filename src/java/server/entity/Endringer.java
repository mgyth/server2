/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import server.web.AdresseController;

/**
 *
 * @author David
 */

@Entity
@XmlRootElement
public class Endringer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private Double currentVersion;
    private Double clientVersion;
    
    private ArrayList <Doerer> doerer;
    private ArrayList <Adresse> adresser;
    private ArrayList <Beslagslister> beslag;
    private ArrayList <Firma> firma;
    private ArrayList <Kategorier> kategorier;
    private ArrayList <KontaktPersoner> kontakter;
    private ArrayList <Prosjekter> prosjekter;
    private ArrayList <Tegninger> tegninger;
    
    public Endringer(){
    }

    public Endringer(Double clientVersion) {
        this.clientVersion = clientVersion;
    }

    public Double getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(Double currentVersion) {
        this.currentVersion = currentVersion;
    }

    public Double getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(Double clientVersion) {
        this.clientVersion = clientVersion;
    }

    public ArrayList<Doerer> getDoerer() {
        return doerer;
    }

    public void setDoerer(ArrayList<Doerer> doerer) {
        this.doerer = doerer;
    }

    public ArrayList<Adresse> getAdresser() {
        return adresser;
    }

    public void setAdresser(ArrayList<Adresse> adresser) {
        this.adresser = adresser;
    }

    public ArrayList<Beslagslister> getBeslag() {
        return beslag;
    }

    public void setBeslag(ArrayList<Beslagslister> beslag) {
        this.beslag = beslag;
    }

    public ArrayList<Firma> getFirma() {
        return firma;
    }

    public void setFirma(ArrayList<Firma> firma) {
        this.firma = firma;
    }

    public ArrayList<Kategorier> getKategorier() {
        return kategorier;
    }

    public void setKategorier(ArrayList<Kategorier> kategorier) {
        this.kategorier = kategorier;
    }

    public ArrayList<KontaktPersoner> getKontakter() {
        return kontakter;
    }

    public void setKontakter(ArrayList<KontaktPersoner> kontakter) {
        this.kontakter = kontakter;
    }

    public ArrayList<Prosjekter> getProsjekter() {
        return prosjekter;
    }

    public void setProsjekter(ArrayList<Prosjekter> prosjekter) {
        this.prosjekter = prosjekter;
    }

    public ArrayList<Tegninger> getTegninger() {
        return tegninger;
    }

    public void setTegninger(ArrayList<Tegninger> tegninger) {
        this.tegninger = tegninger;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (currentVersion != null ? currentVersion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Endringer)) {
            return false;
        }
        Endringer other = (Endringer) object;
        if ((this.currentVersion == null && other.currentVersion != null) || (this.currentVersion != null && !this.currentVersion.equals(other.currentVersion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "server.entity.Endringer[ id=" + currentVersion + " ]";
    }

    
}

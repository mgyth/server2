/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "KontaktPersoner")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KontaktPersoner.findAll", query = "SELECT k FROM KontaktPersoner k"),
    @NamedQuery(name = "KontaktPersoner.findByIdPersoner", query = "SELECT k FROM KontaktPersoner k WHERE k.idPersoner = :idPersoner"),
    @NamedQuery(name = "KontaktPersoner.findByTittel", query = "SELECT k FROM KontaktPersoner k WHERE k.tittel = :tittel"),
    @NamedQuery(name = "KontaktPersoner.findByNavn", query = "SELECT k FROM KontaktPersoner k WHERE k.navn = :navn"),
    @NamedQuery(name = "KontaktPersoner.findByTlf", query = "SELECT k FROM KontaktPersoner k WHERE k.tlf = :tlf"),
    @NamedQuery(name = "KontaktPersoner.findByEpost", query = "SELECT k FROM KontaktPersoner k WHERE k.epost = :epost"),
    @NamedQuery(name = "KontaktPersoner.findByVersion", query = "SELECT k FROM KontaktPersoner k WHERE k.version > :version")})
public class KontaktPersoner implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "idPersoner")
    private Integer idPersoner;
    @Size(max = 65)
    @Column(name = "Tittel")
    private String tittel;
    @Size(max = 65)
    @Column(name = "Navn")
    private String navn;
    @Size(max = 20)
    @Column(name = "Tlf")
    private String tlf;
    @Size(max = 65)
    @Column(name = "Epost")
    private String epost;
//    @JoinTable(name = "ProsjektKontakter", joinColumns = {
//        @JoinColumn(name = "KontaktPersoner_idPersoner", referencedColumnName = "idPersoner")}, inverseJoinColumns = {
//        @JoinColumn(name = "Prosjekter_ProsjektID", referencedColumnName = "ProsjektID")})
//    @ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL )
//    private Collection<Prosjekter> prosjekterCollection;
    @ManyToMany(mappedBy = "kontaktPersonerCollection", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<Prosjekter> prosjekterCollection;
    @JoinColumn(name = "Firma_id", referencedColumnName = "idFirma")
    @ManyToOne(fetch = FetchType.LAZY)
    private Firma firmaid;
    @JoinColumn(name = "Adresse", referencedColumnName = "idAdresse" )
    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Adresse adresse;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public KontaktPersoner() {
    }

    public KontaktPersoner(Integer idPersoner) {
        this.idPersoner = idPersoner;
    }

    public Integer getIdPersoner() {
        return idPersoner;
    }

    public void setIdPersoner(Integer idPersoner) {
        this.idPersoner = idPersoner;
    }

    public String getTittel() {
        return tittel;
    }

    public void setTittel(String tittel) {
        this.tittel = tittel;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getTlf() {
        return tlf;
    }

    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    public String getEpost() {
        return epost;
    }

    public void setEpost(String epost) {
        this.epost = epost;
    }

    @XmlTransient
    public Collection<Prosjekter> getProsjekterCollection() {
        return prosjekterCollection;
    }

    public void setProsjekterCollection(Collection<Prosjekter> prosjekterCollection) {
        this.prosjekterCollection = prosjekterCollection;
    }
    
    public void addProsjekter(Prosjekter prosjekt){
        this.prosjekterCollection.add(prosjekt);
    }
    
    public void removeProsjekter(Prosjekter prosjekt){
        this.prosjekterCollection.remove(prosjekt);
    }

    public Firma getFirmaid() {
        return firmaid;
    }

    public void setFirmaid(Firma firmaid) {
        this.firmaid = firmaid;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersoner != null ? idPersoner.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KontaktPersoner)) {
            return false;
        }
        KontaktPersoner other = (KontaktPersoner) object;
        if ((this.idPersoner == null && other.idPersoner != null) || (this.idPersoner != null && !this.idPersoner.equals(other.idPersoner))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tittel + " " + navn;
    }
    private static final Logger LOG = Logger.getLogger(KontaktPersoner.class.getName());
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Prosjekter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prosjekter.findAll", query = "SELECT p FROM Prosjekter p"),
    @NamedQuery(name = "Prosjekter.findByProsjektID", query = "SELECT p FROM Prosjekter p WHERE p.prosjektID = :prosjektID"),
    @NamedQuery(name = "Prosjekter.findByProsjektNavn", query = "SELECT p FROM Prosjekter p WHERE p.prosjektNavn = :prosjektNavn"),
    @NamedQuery(name = "Prosjekter.findByOpprettet", query = "SELECT p FROM Prosjekter p WHERE p.opprettet = :opprettet"),
    @NamedQuery(name = "Prosjekter.findByOppdatert", query = "SELECT p FROM Prosjekter p WHERE p.oppdatert = :oppdatert"),
    @NamedQuery(name = "Prosjekter.findByVersion", query = "SELECT p FROM Prosjekter p WHERE p.version > :version")})
public class Prosjekter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "ProsjektID")
    private Integer prosjektID;
    @Size(max = 45)
    @Pattern(regexp="[a-z, A-Z, æøåÆØÅ /.,-]+")
    @Column(name = "ProsjektNavn")
    private String prosjektNavn;
    @Lob
    @Size(max = 65)
    @Pattern(regexp="[\\w\\sæøåÆØÅ,./ ]+")
    @Column(name = "ProsjektInfo")
    private String prosjektInfo;
//    Dette er brukerene til prosjektet.
    @JoinTable(name = "ProsjektBrukere", joinColumns = {
        @JoinColumn(name = "Prosjekter", referencedColumnName = "ProsjektID")}, inverseJoinColumns = {
        @JoinColumn(name = "Brukere", referencedColumnName = "idBrukere")})
    @ManyToMany(fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<Brukere> brukereCollection;
    @Column(name = "Opprettet")
    @Temporal(TemporalType.TIMESTAMP)
    private Date opprettet;
    @Column(name = "Oppdatert")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oppdatert;
//    Dette er kontaktpersonene til prosjektet.
    @JoinTable(name = "ProsjektKontakter", joinColumns = {
        @JoinColumn(name = "Prosjekter_ProsjektID", referencedColumnName = "ProsjektID")}, inverseJoinColumns = {
        @JoinColumn(name = "KontaktPersoner_idPersoner", referencedColumnName = "idPersoner")})
    @ManyToMany(fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<KontaktPersoner> kontaktPersonerCollection;
    @OneToMany(mappedBy = "prosjektIDfk", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Collection<Doerer> doererCollection;
    @JoinColumn(name = "Tegning_fk", referencedColumnName = "idTegninger")
    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Tegninger tegningfk;
    @JoinColumn(name = "ProsjektKategorier_idProsjektKategorier", referencedColumnName = "idProsjektKategorier")
    @ManyToOne(fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private ProsjektKategorier prosjektKategorieridProsjektKategorier;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Prosjekter() {
        this.opprettet = new Date();
    }

    public Prosjekter(Integer prosjektID) {
        this.prosjektID = prosjektID;
    }

    public Integer getProsjektID() {
        return prosjektID;
    }

    public void setProsjektID(Integer prosjektID) {
        this.prosjektID = prosjektID;
    }

    public String getProsjektNavn() {
        return prosjektNavn;
    }

    public void setProsjektNavn(String prosjektNavn) {
        this.prosjektNavn = prosjektNavn;
    }

    public String getProsjektInfo() {
        return prosjektInfo;
    }

    public void setProsjektInfo(String prosjektInfo) {
        this.prosjektInfo = prosjektInfo;
    }

    @XmlTransient
    public Collection<Brukere> getBrukereCollection() {
        return brukereCollection;
    }

    public void setBrukereCollection(Collection<Brukere> brukereCollection) {
        this.brukereCollection = brukereCollection;
    }
    
    public void addBrukere(Brukere bruker){
        this.brukereCollection.add(bruker);
    }
    
    public void removeBrukere(Brukere bruker){
        this.brukereCollection.remove(bruker);
    }

    public Date getOpprettet() {
        return opprettet;
    }

    public void setOpprettet(Date opprettet) {
        this.opprettet = opprettet;
    }

    public Date getOppdatert() {
        return oppdatert;
    }

    public void setOppdatert(Date oppdatert) {
        this.oppdatert = oppdatert;
        this.oppdatert = new Date();
    }

    @XmlTransient
    public Collection<KontaktPersoner> getKontaktPersonerCollection() {
        return kontaktPersonerCollection;
    }

    public void setKontaktPersonerCollection(Collection<KontaktPersoner> kontaktPersonerCollection) {
        this.kontaktPersonerCollection = kontaktPersonerCollection;
    }
    
    public void addKontaktPersoner(KontaktPersoner kontaktperson){
        this.kontaktPersonerCollection.add(kontaktperson);
    }
    
    public void removeKontaktPersoner(KontaktPersoner kontaktperson){
        this.kontaktPersonerCollection.remove(kontaktperson);
    }

    @XmlTransient
    public Collection<Doerer> getDoererCollection() {
        return doererCollection;
    }

    public void setDoererCollection(Collection<Doerer> doererCollection) {
        this.doererCollection = doererCollection;
    }
    public void addDoererCollection(Doerer doer){
        this.doererCollection.add(doer);
    }

    public Tegninger getTegningfk() {
        return tegningfk;
    }

    public void setTegningfk(Tegninger tegningfk) {
        this.tegningfk = tegningfk;
    }

    public ProsjektKategorier getProsjektKategorieridProsjektKategorier() {
        return prosjektKategorieridProsjektKategorier;
    }

    public void setProsjektKategorieridProsjektKategorier(ProsjektKategorier prosjektKategorieridProsjektKategorier) {
        this.prosjektKategorieridProsjektKategorier = prosjektKategorieridProsjektKategorier;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prosjektID != null ? prosjektID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prosjekter)) {
            return false;
        }
        Prosjekter other = (Prosjekter) object;
        if ((this.prosjektID == null && other.prosjektID != null) || (this.prosjektID != null && !this.prosjektID.equals(other.prosjektID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "(" + this.prosjektID + ") " + this.prosjektNavn;
    }
    private static final Logger LOG = Logger.getLogger(Prosjekter.class.getName());
    
}

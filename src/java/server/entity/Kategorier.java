/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Kategorier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kategorier.findAll", query = "SELECT k FROM Kategorier k"),
    @NamedQuery(name = "Kategorier.findByIdKategori", query = "SELECT k FROM Kategorier k WHERE k.idKategori = :idKategori"),
    @NamedQuery(name = "Kategorier.findByNavn", query = "SELECT k FROM Kategorier k WHERE k.navn = :navn"),
    @NamedQuery(name = "Kategorier.findByVersion", query = "SELECT k FROM Kategorier k WHERE k.version > :version")})
public class Kategorier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = true)
    @Column(name = "idKategori")
    private Integer idKategori;
    @Size(max = 45)
    @Column(name = "Navn")
    private String navn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kategoriidfk", fetch = FetchType.LAZY)
    private Collection<Beslagslister> beslagslisterCollection;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Kategorier() {
    }

    public Kategorier(Integer idKategori) {
        this.idKategori = idKategori;
    }

    public Integer getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(Integer idKategori) {
        this.idKategori = idKategori;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    @XmlTransient
    public Collection<Beslagslister> getBeslagslisterCollection() {
        return beslagslisterCollection;
    }

    public void setBeslagslisterCollection(Collection<Beslagslister> beslagslisterCollection) {
        this.beslagslisterCollection = beslagslisterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKategori != null ? idKategori.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kategorier)) {
            return false;
        }
        Kategorier other = (Kategorier) object;
        if ((this.idKategori == null && other.idKategori != null) || (this.idKategori != null && !this.idKategori.equals(other.idKategori))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return navn;
    }
    private static final Logger LOG = Logger.getLogger(Kategorier.class.getName());
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Tegninger")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tegninger.findAll", query = "SELECT t FROM Tegninger t"),
    @NamedQuery(name = "Tegninger.findByIdTegninger", query = "SELECT t FROM Tegninger t WHERE t.idTegninger = :idTegninger"),
    @NamedQuery(name = "Tegninger.findByVersion", query = "SELECT t FROM Tegninger t WHERE t.version > :version")})
public class Tegninger implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    
    @Column(name = "idTegninger")
    private Integer idTegninger;
    @Lob
    @Column(name = "Tegning")
    private byte[] tegning;
    @OneToMany(mappedBy = "tegningfk", fetch = FetchType.LAZY)
    private Collection<Prosjekter> prosjekterCollection;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Tegninger() {
    }

    public Tegninger(Integer idTegninger) {
        this.idTegninger = idTegninger;
    }

    public Integer getIdTegninger() {
        return idTegninger;
    }

    public void setIdTegninger(Integer idTegninger) {
        this.idTegninger = idTegninger;
    }

    public byte[] getTegning() {
        return tegning;
    }

    public void setTegning(byte[] tegning) {
        this.tegning = tegning;
    }

    @XmlTransient
    public Collection<Prosjekter> getProsjekterCollection() {
        return prosjekterCollection;
    }

    public void setProsjekterCollection(Collection<Prosjekter> prosjekterCollection) {
        this.prosjekterCollection = prosjekterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTegninger != null ? idTegninger.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tegninger)) {
            return false;
        }
        Tegninger other = (Tegninger) object;
        if ((this.idTegninger == null && other.idTegninger != null) || (this.idTegninger != null && !this.idTegninger.equals(other.idTegninger))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "server.entity.Tegninger[ idTegninger=" + idTegninger + " ]";
    }
    private static final Logger LOG = Logger.getLogger(Tegninger.class.getName());
    
}

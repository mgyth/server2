/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "ProsjektKategorier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProsjektKategorier.findAll", query = "SELECT p FROM ProsjektKategorier p"),
    @NamedQuery(name = "ProsjektKategorier.findByIdProsjektKategorier", query = "SELECT p FROM ProsjektKategorier p WHERE p.idProsjektKategorier = :idProsjektKategorier"),
    @NamedQuery(name = "ProsjektKategorier.findByKategoriNavn", query = "SELECT p FROM ProsjektKategorier p WHERE p.kategoriNavn = :kategoriNavn"),
    @NamedQuery(name = "ProsjektKategorier.findByVersion", query = "SELECT p FROM ProsjektKategorier p WHERE p.version > :version")})
public class ProsjektKategorier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    
    @Column(name = "idProsjektKategorier")
    private Integer idProsjektKategorier;
    @Size(max = 45)
    @Column(name = "KategoriNavn")
    private String kategoriNavn;
    @OneToMany(mappedBy = "prosjektKategorieridProsjektKategorier", fetch = FetchType.LAZY)
    private Collection<Prosjekter> prosjekterCollection;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public ProsjektKategorier() {
    }

    public ProsjektKategorier(Integer idProsjektKategorier) {
        this.idProsjektKategorier = idProsjektKategorier;
    }

    public Integer getIdProsjektKategorier() {
        return idProsjektKategorier;
    }

    public void setIdProsjektKategorier(Integer idProsjektKategorier) {
        this.idProsjektKategorier = idProsjektKategorier;
    }

    public String getKategoriNavn() {
        return kategoriNavn;
    }

    public void setKategoriNavn(String kategoriNavn) {
        this.kategoriNavn = kategoriNavn;
    }

    @XmlTransient
    public Collection<Prosjekter> getProsjekterCollection() {
        return prosjekterCollection;
    }

    public void setProsjekterCollection(Collection<Prosjekter> prosjekterCollection) {
        this.prosjekterCollection = prosjekterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProsjektKategorier != null ? idProsjektKategorier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProsjektKategorier)) {
            return false;
        }
        ProsjektKategorier other = (ProsjektKategorier) object;
        if ((this.idProsjektKategorier == null && other.idProsjektKategorier != null) || (this.idProsjektKategorier != null && !this.idProsjektKategorier.equals(other.idProsjektKategorier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return kategoriNavn;
    }
    private static final Logger LOG = Logger.getLogger(ProsjektKategorier.class.getName());
    
}


package server.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Version")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Version.findAll", query = "SELECT v FROM Version v"),
    @NamedQuery(name = "Version.findByVersion", query = "SELECT v FROM Version v WHERE v.version = :version"),
    @NamedQuery(name = "Version.findByVersionID", query = "SELECT v FROM Version v WHERE v.versionID = :versionID"),
    @NamedQuery(name = "Version.findByTimestamp", query = "SELECT v FROM Version v WHERE v.timestamp = :timestamp")})
public class Version implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "version")
    private Double version;
    @Id
    @Basic(optional = false)
    
    @Column(name = "VersionID")
    private Integer versionID;
    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    public Version() {
    }

    public Version(Integer versionID) {
        this.versionID = versionID;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Integer getVersionID() {
        return versionID;
    }

    public void setVersionID(Integer versionID) {
        this.versionID = versionID;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (versionID != null ? versionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Version)) {
            return false;
        }
        Version other = (Version) object;
        if ((this.versionID == null && other.versionID != null) || (this.versionID != null && !this.versionID.equals(other.versionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "server.entity.Version[ versionID=" + versionID + " ]";
    }
    private static final Logger LOG = Logger.getLogger(Version.class.getName());

}

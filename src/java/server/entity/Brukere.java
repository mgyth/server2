/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mikael
 */
@Entity
@Table(name = "Brukere")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Brukere.findAll", query = "SELECT b FROM Brukere b"),
    @NamedQuery(name = "Brukere.findByIdBrukere", query = "SELECT b FROM Brukere b WHERE b.idBrukere = :idBrukere"),
    @NamedQuery(name = "Brukere.findByBrukernavn", query = "SELECT b FROM Brukere b WHERE b.brukernavn = :brukernavn"),
    @NamedQuery(name = "Brukere.findByFornavn", query = "SELECT b FROM Brukere b WHERE b.fornavn = :fornavn"),
    @NamedQuery(name = "Brukere.findByEtternavn", query = "SELECT b FROM Brukere b WHERE b.etternavn = :etternavn"),
    @NamedQuery(name = "Brukere.findByPassord", query = "SELECT b FROM Brukere b WHERE b.passord = :passord")})
public class Brukere implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idBrukere")
    private Integer idBrukere;
    @Size(max = 45)
    @Column(name = "Brukernavn")
    private String brukernavn;
    @Size(max = 45)
    @Column(name = "Fornavn")
    private String fornavn;
    @Size(max = 45)
    @Column(name = "Etternavn")
    private String etternavn;
    @Size(max = 45)
    @Column(name = "Passord")
    private String passord;
    @OneToMany(mappedBy = "montertavfk", fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<Beslagslister> beslagslisterCollection;
    @ManyToMany(mappedBy = "brukereCollection", fetch = FetchType.LAZY, cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private Collection<Prosjekter> prosjekterCollection;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Version")
    private Double version;

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public Brukere() {
    }
    
    @XmlTransient
    public Collection<Prosjekter> getProsjekterCollection() {
        return prosjekterCollection;
    }

    public void setProsjekterCollection(Collection<Prosjekter> prosjekterCollection) {
        this.prosjekterCollection = prosjekterCollection;
    }
    
    public void addProsjekter(Prosjekter prosjekt){
        this.prosjekterCollection.add(prosjekt);
    }
    
    public void removeProsjekter(Prosjekter prosjekt){
        this.prosjekterCollection.remove(prosjekt);
    }

    public Brukere(Integer idBrukere) {
        this.idBrukere = idBrukere;
    }

    public Integer getIdBrukere() {
        return idBrukere;
    }

    public void setIdBrukere(Integer idBrukere) {
        this.idBrukere = idBrukere;
    }

    public String getFornavn() {
        return fornavn;
    }

    public void setFornavn(String fornavn) {
        this.fornavn = fornavn;
    }

    public String getEtternavn() {
        return etternavn;
    }

    public void setEtternavn(String etternavn) {
        this.etternavn = etternavn;
    }

    public String getBrukernavn() {
        return brukernavn;
    }

    public void setBrukernavn(String brukernavn) {
        this.brukernavn = brukernavn;
    }

    public String getPassord() {
        return passord;
    }

    public void setPassord(String passord) {
        this.passord = passord;
    }

    @XmlTransient
    public Collection<Beslagslister> getBeslagslisterCollection() {
        return beslagslisterCollection;
    }

    public void setBeslagslisterCollection(Collection<Beslagslister> beslagslisterCollection) {
        this.beslagslisterCollection = beslagslisterCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBrukere != null ? idBrukere.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Brukere)) {
            return false;
        }
        Brukere other = (Brukere) object;
        if ((this.idBrukere == null && other.idBrukere != null) || (this.idBrukere != null && !this.idBrukere.equals(other.idBrukere))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return brukernavn;
    }
    private static final Logger LOG = Logger.getLogger(Brukere.class.getName());



    
}

package server.web;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import server.ejb.BeslagslisterFacade;
import server.entity.Beslagslister;
import server.entity.Doerer;
import server.web.util.JsfUtil;
import server.web.util.PaginationHelper;
import server.web.util.SerializableListDataModel;

@ManagedBean(name = "beslagslisterController")
@SessionScoped
public class BeslagslisterController implements Serializable {

    private Beslagslister current;
    private Beslagslister currentAjax;
    private SerializableListDataModel items = null;
    @EJB
    private server.ejb.BeslagslisterFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public BeslagslisterController() {
    }
    
    public void prepareDelete(Beslagslister beslag){
        this.currentAjax = beslag;
    }

    public Beslagslister getSelected() {
        if (current == null) {
            current = new Beslagslister();
            selectedItemIndex = -1;
        }
        return current;
    }

    private BeslagslisterFacade getFacade() {
        return ejbFacade;
    }

    public void createBeslagFromFile(Beslagslister beslag) {
        getFacade().create(beslag);
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public SerializableListDataModel createPageDataModel() {
                    return new SerializableListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "/beslagslister/List";
    }

    public String prepareView() {
        current = (Beslagslister) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/beslagslister/View";
    }
    
    public void prepareViewProsjekt(Beslagslister beslag) {
        current = beslag;
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();

    }

    public String prepareCreate() {
        current = new Beslagslister();
        selectedItemIndex = -1;
        return "/beslagslister/Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("BeslagslisterUpdated")));
            return prepareCreate();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            return null;
        }
    }
    
    public void prepareCreateAjax(Doerer doren) {
        current = new Beslagslister();
        current.setDoridfk(doren);
        doren.addBeslagslisterCollection(current);
        selectedItemIndex = -1;
    }

    public void createAjax() {
        try {
            getFacade().create(current);
            recreateModel();
//            current.getDoridfk().getBeslagslisterCollection().add(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("BeslagslisterUpdated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }
    
    public void newProjectView() {
        current = null;
    }

    public String prepareEdit() {
        current = (Beslagslister) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/beslagslister/Edit";
    }
    
    public void updateDialog() {
        try {
            getFacade().edit(current);
            recreateModel();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererUpdated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    public String update() {
        try {
            getFacade().edit(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("BeslagslisterUpdated")));
            return "/beslagslister/View";
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            return null;
        }
    }

    /**
     * Sletter
     * beslaget,
     * setter
     * index
     * til
     * -1
     * og
     * ombygger
     * pagineringen
     * og
     * modellen
     *
     * @param
     * Beslagslister
     * som
     * skal
     * slettes
     */
    public void destroyAjax(Beslagslister beslag) {
        current = beslag;
        performDestroy();
        beslag.getDoridfk().getBeslagslisterCollection().remove(beslag);
        recreatePagination();
        recreateModel();
    }

    public String destroy() {
        current = (Beslagslister) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/beslagslister/List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/beslagslister/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/beslagslister/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("BeslagslisterDeleted")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/beslagslister/List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/beslagslister/List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Beslagslister.class)
    public static class BeslagslisterControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BeslagslisterController controller = (BeslagslisterController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "beslagslisterController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Beslagslister) {
                Beslagslister o = (Beslagslister) object;
                return getStringKey(o.getIDBeslagsliste());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Beslagslister.class.getName());
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(BeslagslisterController.class.getName());
}

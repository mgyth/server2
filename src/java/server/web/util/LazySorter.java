
package server.web.util;

/**
 *
 * @author Mikael
 */
import java.util.Comparator;
import java.util.logging.Logger;
import org.primefaces.model.SortOrder;
import server.entity.Prosjekter;

public class LazySorter implements Comparator<Prosjekter> {

    private String sortField;
    
    private SortOrder sortOrder;
    
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Prosjekter pro1, Prosjekter pro2) {
        try {
            Object value1 = Prosjekter.class.getField(this.sortField).get(pro1);
            Object value2 = Prosjekter.class.getField(this.sortField).get(pro2);

            int value = ((Comparable)value1).compareTo(value2);
            
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException();
        }
    }
    private static final Logger LOG = Logger.getLogger(LazySorter.class.getName());
}

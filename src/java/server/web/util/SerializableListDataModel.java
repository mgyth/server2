package server.web.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.DataModel;
import javax.faces.model.DataModelEvent;
import javax.faces.model.DataModelListener;

/**
 *
 * @author Mikael
 */
public class SerializableListDataModel extends DataModel implements Serializable {
    private int rowIndex = 0;
    private List data;
    
    public SerializableListDataModel(){
        this(new ArrayList());
    }

    public SerializableListDataModel(List list) {
 		if (list == null) {
 			list = new ArrayList();
 		}
 		setWrappedData(list);
 	}
 
    @Override
 	public int getRowCount() {
 		return data.size();
 	}
 
    @Override
 	public Object getRowData() {
 		Assert.isTrue(isRowAvailable(), getClass()
 				+ " is in an illegal state - no row is available at the current index.");
 		return data.get(rowIndex);
 	}
 
    @Override
 	public int getRowIndex() {
 		return rowIndex;
 	}
 
    @Override
 	public Object getWrappedData() {
 		return data;
 	}
 
    @Override
 	public boolean isRowAvailable() {
 		return rowIndex >= 0 && rowIndex < data.size();
 	}
 
    @Override
 	public void setRowIndex(int newRowIndex) {
 		if (newRowIndex < -1) {
 			throw new IllegalArgumentException("Illegal row index for " + getClass() + ": " + newRowIndex);
 		}
 		int oldRowIndex = rowIndex;
 		rowIndex = newRowIndex;
 		if (data != null && oldRowIndex != rowIndex) {
 			Object row = isRowAvailable() ? getRowData() : null;
 			DataModelEvent event = new DataModelEvent(this, rowIndex, row);
 			DataModelListener[] listeners = getDataModelListeners();
			for (int i = 0; i < listeners.length; i++) {
				listeners[i].rowSelected(event);
			}
		}
	}

    @Override
 	public void setWrappedData(Object data) {
 		if (data == null) {
 			data = new ArrayList();
		}
 		Assert.isInstanceOf(List.class, data, "The data object for " + getClass() + " must be a List");
 		this.data = (List) data;
		int newRowIndex = 0;
 		setRowIndex(newRowIndex);
	}

    @Override
	public String toString() {
		return data.toString();
	}

}

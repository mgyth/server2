package server.web;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import server.ejb.AdresseFacade;
import server.ejb.KontaktPersonerFacade;
import server.entity.Adresse;
import server.entity.Firma;
import server.entity.KontaktPersoner;
import server.web.util.JsfUtil;
import server.web.util.PaginationHelper;
import server.web.util.SerializableListDataModel;

@ManagedBean(name = "kontaktPersonerController")
@SessionScoped
public class KontaktPersonerController implements Serializable {

    private KontaktPersoner current;
    private DataModel items = null;
    @EJB
    private server.ejb.KontaktPersonerFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private KontaktPersoner currentDialog;
    private ListDataModel allItems;
    
    private String adr1 = null;
    private String adr2 = null;
    private String adr3 = null;
    private Integer postNr = null;
    private String postSted = null;

    public String getAdr1() {
        return adr1;
    }

    public void setAdr1(String adr1) {
        this.adr1 = adr1;
    }

    public String getAdr2() {
        return adr2;
    }

    public void setAdr2(String adr2) {
        this.adr2 = adr2;
    }

    public String getAdr3() {
        return adr3;
    }

    public void setAdr3(String adr3) {
        this.adr3 = adr3;
    }

    public Integer getPostNr() {
        return postNr;
    }

    public void setPostNr(Integer postNr) {
        this.postNr = postNr;
    }

    public String getPostSted() {
        return postSted;
    }

    public void setPostSted(String postSted) {
        this.postSted = postSted;
    }
    
    //@EJB AdresseController aController;

    public KontaktPersoner getCurrentDialog() {
        return currentDialog;
    }

    public void setCurrentDialog(KontaktPersoner currentDialog) {
        this.currentDialog = currentDialog;
    }

    public KontaktPersonerController() {
    }

    public KontaktPersoner getSelected() {
        if (current == null) {
            current = new KontaktPersoner();
            selectedItemIndex = -1;
        }
        return current;
    }

    private KontaktPersonerFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public SerializableListDataModel createPageDataModel() {
                    return new SerializableListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "/kontaktPersoner/List";
    }
    public void prepareListAdd() {
        recreateModel();
    }

    public String prepareView() {
        current = (KontaktPersoner) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/kontaktPersoner/View";
    }
    

    public String prepareCreate() {
        current = new KontaktPersoner();
        selectedItemIndex = -1;
        return "/kontaktPersoner/List";
    }
    
    
    public void prepareCreateDialog() {
        adr1 = null;
        adr2 = null;
        adr3 = null;
        postNr = null;
        postSted = null;
        currentDialog = new KontaktPersoner();
        selectedItemIndex = -1;
       
    }

    public String create() {
        try {
            getFacade().create(currentDialog);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("KontaktPersonerCreated"));
            recreateModel();
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    @EJB AdresseFacade aFacade;
    public void createDialog() {
        Adresse adresse = new Adresse();
        adresse.setAdr1(adr1);
        adresse.setAdr2(adr2);
        adresse.setAdr3(adr3);
        adresse.setPostNr(postNr);
        adresse.setPostSted(postSted);
        //aFacade.create(adresse);
        currentDialog.setAdresse(adresse);
        try {
            getFacade().create(currentDialog);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("KontaktPersonerCreated"));
            recreateModel();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            
        }
    }

    public String prepareEdit() {
        current = (KontaktPersoner) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/kontaktPersoner/Edit";
    }

    public void prepareEditDialog(KontaktPersoner person) {
        currentDialog = person;
//        return "/kontaktPersoner/EditDialog";
    }

    public void updateDialog() {
        try {
            getFacade().edit(currentDialog);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("KontaktPersonerUpdated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured")));
        }
    }

    

    public String update() {
        try {
            getFacade().edit(current);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("KontaktPersonerUpdated")));
            return "/kontaktPersoner/View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (KontaktPersoner) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/kontaktPersoner/List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/kontaktPersoner/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/kontaktPersoner/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("KontaktPersonerDeleted")));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }
    
    public DataModel getAllItems(){
        return allItems = new ListDataModel(ejbFacade.findAll());
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/kontaktPersoner/List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/kontaktPersoner/List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public String getKontaktName(int id) {
        current = ejbFacade.find(id);
        return current.getNavn();
    }

    public String getFirmaNavn(Firma id) {
        return id.getFirmanavn();
    }

    @FacesConverter(forClass = KontaktPersoner.class)
    public static class KontaktPersonerControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            KontaktPersonerController controller = (KontaktPersonerController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "kontaktPersonerController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof KontaktPersoner) {
                KontaktPersoner o = (KontaktPersoner) object;
                return getStringKey(o.getIdPersoner());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + KontaktPersoner.class.getName());
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(KontaktPersonerController.class.getName());
}

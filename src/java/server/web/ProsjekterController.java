package server.web;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import server.ejb.ProsjekterFacade;
import server.entity.Brukere;
import server.entity.KontaktPersoner;
import server.entity.Prosjekter;
import server.web.util.JsfUtil;
import server.web.util.PaginationHelper;
import server.web.util.SerializableListDataModel;

/**
 *
 * @author
 * Mikael
 */
@ManagedBean(name = "prosjekterController")
@SessionScoped
public class ProsjekterController implements Serializable {

    private Prosjekter current;
    private SerializableListDataModel items = null;
    @EJB
    private server.ejb.ProsjekterFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private List<Prosjekter> filteredProsjekter;
//    private LazyDataModel<Prosjekter> lazyModel;
/*
     public LazyDataModel<Prosjekter> getLazyModel() {
     List<Prosjekter> prosjekter = ejbFacade.findAll();
     lazyModel = new LazyProsjekterDataModel(prosjekter);
     return lazyModel;
     }
     */

    public List<Prosjekter> getFilteredProsjekter() {
        return filteredProsjekter;
    }

    public void setFilteredProsjekter(List<Prosjekter> filteredProsjekter) {
        this.filteredProsjekter = filteredProsjekter;
    }

    /**
     *
     */
    public ProsjekterController() {
    }

    /**
     * Returnerer
     * det
     * valgte
     * objektet
     * fra
     * JSF
     * filen.
     * Hvis
     * Objetet
     * er
     * null
     * opprettes
     * et
     * nytt
     * Objekt
     * og
     * selecetedItemIndex
     * settes
     * til
     * den
     * ugyldige
     * verdien
     * -1.
     *
     * @return
     * current,
     * Det
     * valgte
     * Prosjekter
     * objektet
     * eller
     * ett
     * nytt
     * Prosjekter
     * objekt
     */
    public Prosjekter getSelected() {
        if (current == null) {
//            current = new Prosjekter();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ProsjekterFacade getFacade() {
        return ejbFacade;
    }

    /**
     *
     * @return
     */
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(1000) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public SerializableListDataModel createPageDataModel() {
                    return new SerializableListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    /**
     *
     * @return
     */
    public String prepareList() {
        recreateModel();
        return "/prosjekter/List";
    }

    /**
     *
     * @return
     */
    public String prepareView() {
        current = (Prosjekter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/prosjekter/View";
    }

    /**
     *
     * @return
     */
    public String prepareNewView(Prosjekter prosjekt) {
        current = prosjekt;
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/prosjekter/View";
    }

    /**
     *
     * @return
     */
    public String prepareCreate() {
        current = new Prosjekter();
        selectedItemIndex = -1;
        return "/prosjekter/Create";
    }

    /**
     *
     * @return
     */
    public String create() {
        try {
            getFacade().create(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterCreated")));
            return prepareCreate();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));

            return null;
        }
    }

    /**
     *
     * @return
     */
    public String prepareEdit() {
        current = (Prosjekter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/prosjekter/Edit";
    }

    public void addKontaktPerson(KontaktPersoner kontaktperson) {
        if (!current.getKontaktPersonerCollection().contains(kontaktperson)) {
            current.addKontaktPersoner(kontaktperson);
            kontaktperson.addProsjekter(current);
            try {
                getFacade().editKontaktPerson(kontaktperson);
                getFacade().edit(current);
                recreateModel();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterKontaktPersonerFinnes")));
        }
    }

    public void removeFromProject(KontaktPersoner kontaktperson, Prosjekter prosjekt) {
        kontaktperson.removeProsjekter(prosjekt);
        prosjekt.removeKontaktPersoner(kontaktperson);
        try {
            getFacade().edit(prosjekt);
            getFacade().editKontaktPerson(kontaktperson);
            recreateModel();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured")));
        }
    }

    public void addBruker(Brukere bruker) {
        if (!current.getBrukereCollection().contains(bruker)) {
            current.addBrukere(bruker);
            bruker.addProsjekter(current);
            try {
                getFacade().editBrukere(bruker);
                getFacade().edit(current);
                recreateModel();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterBrukerFinnes")));
        }
    }

    public void removeBruker(Brukere bruker) {
        if (current.getBrukereCollection().contains(bruker)) {
            current.removeBrukere(bruker);
            bruker.removeProsjekter(current);
            try {
                getFacade().editBrukere(bruker);
                getFacade().edit(current);
                recreateModel();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
            } catch (Exception e) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterBrukerFinnesIkke")));
        }
    }

    /**
     *
     * @return
     */
    public String update() {
        try {
            try {
                getFacade().edit(current);
            } catch (ConstraintViolationException ex) {
                Set<ConstraintViolation<?>> violation = ex.getConstraintViolations();
                String violations = "";
                for (Iterator it = violation.iterator(); it.hasNext();) {
                    String viol = (String) it.next();
                    violations += viol + "\n";
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Database: ", violations));
                Logger.getLogger(ProsjekterController.class.getName()).log(Level.SEVERE, null, ex);
            }

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
            return "/prosjekter/View";
        } catch (EJBException ejx) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Database: ", ejx.getMessage() + "hahah"));
            return null;
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            return null;
        }
    }

    public void updateDialog() {

        try {
            getFacade().edit(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
        } catch (ConstraintViolationException ex) {
            Set<ConstraintViolation<?>> violation = ex.getConstraintViolations();
            String violations = "";
            for (Iterator it = violation.iterator(); it.hasNext();) {
                String viol = (String) it.next();
                violations += viol + "\n";
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Database: ", violations));
            Logger.getLogger(ProsjekterController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EJBException ejx) {
            ejx.getCause().getLocalizedMessage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Database: ",
                    ejx.getCause()
                    .getCause()
                    .getMessage()));

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    /**
     *
     * @return
     */
    public String destroy() {
        current = (Prosjekter) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/prosjekter/List";
    }

    /**
     * Sletter
     * prosjektet,
     * setter
     * index
     * til
     * -1
     * og
     * ombygger
     * pagineringen
     * og
     * modellen
     *
     * @param
     * Prosjekt
     * som
     * skal
     * slettes
     */
    public void destroyAjax(Prosjekter prosjekt) {
        RequestContext context = RequestContext.getCurrentInstance();
        boolean slettet = false; // sett ferdig til false
        current = prosjekt;
        performDestroy();

        recreatePagination();
        recreateModel();

        context.addCallbackParam("slettet", slettet);
    }

    /**
     *
     * @return
     */
    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/prosjekter/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/prosjekter/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterDeleted")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    /**
     *
     * @return
     */
    public DataModel getItems() {
        if (items == null) {
            if (getPagination().createPageDataModel() instanceof DataModel) {
                items = getPagination().createPageDataModel();
            }
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    /**
     *
     * @return
     */
    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/prosjekter/List";
    }

    /**
     *
     * @return
     */
    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/prosjekter/List";
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    /**
     *
     * @return
     */
    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    /**
     *
     */
    @FacesConverter(forClass = Prosjekter.class)
    public static class ProsjekterControllerConverter implements Converter {

        /**
         *
         * @param
         * facesContext
         * @param
         * component
         * @param
         * value
         * @return
         */
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProsjekterController controller = (ProsjekterController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "prosjekterController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuffer sb = new StringBuffer();
            sb.append(value);
            return sb.toString();
        }

        /**
         *
         * @param
         * facesContext
         * @param
         * component
         * @param
         * object
         * @return
         */
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Prosjekter) {
                Prosjekter o = (Prosjekter) object;
                return getStringKey(o.getProsjektID());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Prosjekter.class.getName());
            }
        }
    }
}

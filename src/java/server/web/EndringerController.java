/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server.web;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import server.entity.Endringer;
import server.entity.Version;

/**
 *
 * @author David
 */
public class EndringerController implements Serializable{
    private Endringer endring;
    private EntityManager em;

    public EndringerController() {
    }
    
    public Endringer getEndringer(double version) {
        endring = new Endringer(version);
        endring.setCurrentVersion(((Version)em.createNamedQuery("Version.findByVersionID").setParameter("versionID", 1).getSingleResult()).getVersion());
        
        endring.setAdresser((ArrayList)em.createNamedQuery("Adresse.findByVersion").setParameter("version", version).getResultList());
        endring.setBeslag((ArrayList)em.createNamedQuery("Beslagslister.findByVersion").setParameter("version", version).getResultList());
        endring.setDoerer((ArrayList)em.createNamedQuery("Doerer.findByVersion").setParameter("version", version));
        endring.setFirma((ArrayList)em.createNamedQuery("Firma.findByVersion").setParameter("version", version));
        endring.setKategorier((ArrayList)em.createNamedQuery("Kategorier.findByVersion").setParameter("version", version));
        endring.setKontakter((ArrayList)em.createNamedQuery("KontaktPersoner.findByVersion").setParameter("version", version));
        endring.setProsjekter((ArrayList)em.createNamedQuery("Prosjekter.findByVersion").setParameter("version", version));
        endring.setTegninger((ArrayList)em.createNamedQuery("Tegninger.findByVersion").setParameter("version", version));
        return endring;
    }
}

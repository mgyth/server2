package server.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import server.entity.Prosjekter;
import server.web.util.LazySorter;

/**
 *
 * @author
 * Mikael
 */
@ManagedBean(name = "lazyProsjekterDataModel")
@SessionScoped
public class LazyProsjekterDataModel extends LazyDataModel<Prosjekter> {

    private List<Prosjekter> datasource;

    public LazyProsjekterDataModel(List<Prosjekter> datasource) {
        this.datasource = datasource;
    }

    @Override
    public Prosjekter getRowData(String rowKey) {
        for (Prosjekter prosjekt : datasource) {
            if (prosjekt.getProsjektID().toString().equals(rowKey)) {
                return prosjekt;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Prosjekter prosjekt) {
        return prosjekt.getProsjektID().toString();
    }

    @Override
    public List<Prosjekter> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        List<Prosjekter> data = new ArrayList<>();

        //filter  
        for (Prosjekter prosjekt : datasource) {
            boolean match = true;

            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = filters.get(filterProperty);
                    String fieldValue = String.valueOf(prosjekt.getClass().getField(filterProperty).get(prosjekt));

                    if (filterValue == null || fieldValue.startsWith(filterValue)) {
                        match = true;
                    } else {
                        match = false;
                        break;
                    }
                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                    match = false;
                }
            }

            if (match) {
                data.add(prosjekt);
            }
        }

        //sort  
        if (sortField != null) {
            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }

        //rowCount  
        int dataSize = data.size();
        this.setRowCount(dataSize);

        //paginate  
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            } catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        } else {
            return data;
        }
    }
    private static final Logger LOG = Logger.getLogger(LazyProsjekterDataModel.class.getName());
}

package server.web;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import server.ejb.BrukereFacade;
import server.entity.Brukere;
import server.entity.Prosjekter;
import server.web.util.JsfUtil;
import server.web.util.PaginationHelper;
import server.web.util.SerializableListDataModel;

@ManagedBean(name = "brukereController")
@SessionScoped
public class BrukereController implements Serializable {

    private Brukere current;
    private Brukere primeCurrent;
    private SerializableListDataModel items = null;
    @EJB
    private server.ejb.BrukereFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private String gammeltPass;
    private String nyttPass;
    private String nyttPassKontroll;
    

    public BrukereController() {
    }

    public Brukere getPrimeCurrent() {
        return primeCurrent;
    }

    public void setPrimeCurrent(Brukere primeCurrent) {
        this.primeCurrent = primeCurrent;
        this.current = primeCurrent;
    }
    

    public String getGammeltPass() {
        return gammeltPass;
    }

    public void setGammeltPass(String gammeltPass) {
        this.gammeltPass = gammeltPass;
    }

    public String getNyttPass() {
        return nyttPass;
    }

    public void setNyttPass(String nyttPass) {
        this.nyttPass = nyttPass;
    }

    public String getNyttPassKontroll() {
        return nyttPassKontroll;
    }

    public void setNyttPassKontroll(String nyttPassKontroll) {
        this.nyttPassKontroll = nyttPassKontroll;
    }
    
    
    public Brukere getSelected() {
        if (current == null) {
            current = new Brukere();
            selectedItemIndex = -1;
        }
        return current;
    }

    private BrukereFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public SerializableListDataModel createPageDataModel() {
                    return new SerializableListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public void addProsjekt(Prosjekter prosjekt) {
        Prosjekter pro = prosjekt;
        for(Brukere br : prosjekt.getBrukereCollection()){
            br.addProsjekter(prosjekt);
        }
        
//        try {
//            bruker.addProsjekter(prosjekt);
//            getFacade().edit(bruker);
//            prosjekt.addBrukere(bruker);
//            getFacade().editProsjekt(prosjekt);
//            
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("ProsjekterUpdated")));
//        } catch (Exception e) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
//        }


    }

    public String prepareList() {
        recreateModel();
        return "/brukere/List";
    }
    
    public void reset(){
        resetFields();
        current = null;
        recreateModel();
    }
    
    public void prepareEditDialog(Brukere brukerItem) {
        current = brukerItem;
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        resetFields();
    }

    public String prepareView() {
        current = (Brukere) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/brukere/View";
    }
/*      Kan denne slettes?
    public String prepareCreate() {
        current = new Brukere();
        selectedItemIndex = -1;
        this.gammeltPass = null;
        this.nyttPass = null;
        this.nyttPassKontroll = null;
        return "/brukere/Create";
    }
    */
    public void prepareCreateDialog() {
        current = new Brukere();
        selectedItemIndex = -1;
        resetFields();
    }
/*
    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    */
    public void createDialog() {
        if (validateInput(false)){
            try {
                getFacade().create(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereCreated"));
                recreateModel();
                prepareCreateDialog();
            } catch (Exception e) {
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
        resetFields();
            
    }

    public void prepareEdit() {
        current = (Brukere) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        resetFields();
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereUpdated"));
            return "/brukere/View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public void updateDialog() {
        if (validateInput(true)) {
            try {
                getFacade().edit(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereUpdated"));
                recreateModel();
            } catch (Exception e) {
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
        resetFields();
        recreateModel();
            
    }

    public String destroy() {
        current = (Brukere) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/brukere/List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/brukere/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/brukere/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/brukere/List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/brukere/List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    private boolean validateInput(boolean edit) {
        boolean validInput = true;
        if (( current.getBrukernavn() == null) || (current.getBrukernavn().length() < 1)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereTomtNavn"));
            validInput = false;
        }
        if (!nyttPass.equals(nyttPassKontroll)) {
            JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("BrukerePassordKontroll"));
            validInput = false;
        }
        if (!edit){
            if (nyttPass.length() < 6) {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("BrukerePassordLengde"));
                validInput = false;
            } else {
                current.setPassord(nyttPass);
            }
        } else {
            if (!current.getPassord().equals(gammeltPass)) {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("BrukereFeilPassord"));
                validInput = false;
            }
            if (((nyttPass.length() < 6) && (nyttPass.length() > 0)) || (nyttPass.length() != 0)) {
                JsfUtil.addErrorMessage(ResourceBundle.getBundle("/Bundle").getString("BrukerePassordLengde"));
                validInput = false;
            } else {
                if (nyttPass.length() != 0) {
                    current.setPassord(nyttPass);
                }
                
            }
        }
        return validInput;
    }

    private void resetFields() {
        this.gammeltPass = null;
        this.nyttPass = null;
        this.nyttPassKontroll = null;
    }

    @FacesConverter(forClass = Brukere.class)
    public static class BrukereControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BrukereController controller = (BrukereController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "brukereController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Brukere) {
                Brukere o = (Brukere) object;
                return getStringKey(o.getIdBrukere());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Brukere.class.getName());
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(BrukereController.class.getName());
}

package server.web;

import java.io.Serializable;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.xml.bind.annotation.XmlRootElement;
import server.ejb.DoererFacade;
import server.entity.Doerer;
import server.entity.Prosjekter;
import server.web.util.JsfUtil;
import server.web.util.PaginationHelper;
import server.web.util.SerializableListDataModel;

@ManagedBean(name = "doererController")
@SessionScoped
public class DoererController implements Serializable {

    private Doerer current;
    private Doerer currentAjax;
    private SerializableListDataModel items = null;
    @EJB
    private server.ejb.DoererFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public DoererController() {
    }

    public Doerer getCurrentAjax() {
        return currentAjax;
    }

    public void prepareDelete(Doerer currentAjax) {
        this.currentAjax = currentAjax;
    }
    
    

    public Doerer getSelected() {
        if (current == null) {
            current = new Doerer();
            selectedItemIndex = -1;
        }
        return current;
    }

    private DoererFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public SerializableListDataModel createPageDataModel() {
                    return new SerializableListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "/doerer/List";
    }

    public String prepareView() {
        current = (Doerer) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/doerer/View";
    }

    public void newProjectView() {
        current = null;
    }

    public void prepareViewProsjekt(Doerer doererItem) {
        current = doererItem;
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();

    }

    public String prepareCreate() {
        current = new Doerer();
        selectedItemIndex = -1;
        return "/doerer/Create";
    }
    
    public void prepareCreateAjax(Prosjekter prosjekt) {
        current = new Doerer();
        current.setProsjektIDfk(prosjekt);
        prosjekt.addDoererCollection(current);
        selectedItemIndex = -1;
    }

    public void createAjax() {
        try {
            getFacade().create(current);
            recreateModel();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererCreated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            
        }
    }
    
    public String create() {
        try {
            getFacade().create(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererCreated")));
            return prepareCreate();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Doerer) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "/doerer/Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererUpdated")));
            return "/doerer/View";
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
            return null;
        }
    }

    public String destroy() {
        current = (Doerer) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "/doerer/List";
    }
    
    /**
     * Sletter døren, setter index til -1 og ombygger pagineringen og modellen
     * @param Doerer som skal slettes
     */
    public void destroyAjax() {
        performDestroy(currentAjax);
        currentAjax.getProsjektIDfk().getDoererCollection().remove(currentAjax);
        recreatePagination();
        recreateModel();
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "/doerer/View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "/doerer/List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererDeleted")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }
    private void performDestroy(Doerer doren) {
        try {
            getFacade().remove(doren);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererDeleted")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public void updateDialog() {
        try {
            getFacade().edit(current);
            recreateModel();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Database: ", ResourceBundle.getBundle("/Bundle").getString("DoererUpdated")));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Database: ", ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured") + " " + e));
        }
    }

    public DataModel getItems() {
        if (items == null) {
            if (getPagination().createPageDataModel() instanceof DataModel) {
                items = getPagination().createPageDataModel();
            }
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "/doerer/List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "/doerer/List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    @FacesConverter(forClass = Doerer.class)
    public static class DoererControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DoererController controller = (DoererController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "doererController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Doerer) {
                Doerer o = (Doerer) object;
                return getStringKey(o.getIDDor());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Doerer.class.getName());
            }
        }
    }
    private static final Logger LOG = Logger.getLogger(DoererController.class.getName());
}
